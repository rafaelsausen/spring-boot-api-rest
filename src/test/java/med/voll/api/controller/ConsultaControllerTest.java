package med.voll.api.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import med.voll.api.domain.consulta.AgendaDeConsultas;
import med.voll.api.domain.consulta.DadosAgendamentoConsulta;
import med.voll.api.domain.consulta.DadosDetalhamentoConsulta;
import med.voll.api.domain.medico.Especialidade;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import java.time.LocalDateTime;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
class ConsultaControllerTest {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private JacksonTester<DadosAgendamentoConsulta> dadosAgendamentoConsultaJson;
    @Autowired
    private JacksonTester<DadosDetalhamentoConsulta> dadosDetalhamentoConsultaJson;
    @MockBean
    private AgendaDeConsultas agendaDeConsultas;

    @Test
    @DisplayName("Deveria devolver código HTTP 400 quando informações estão inválidas")
    @WithMockUser
    void testAgendarCenario1() throws Exception {
        MockHttpServletResponse response = mvc.perform(post("/consultas")).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
    }

    @Test
    @DisplayName("Deveria devolver código HTTP 200 quando informações estão válidas")
    @WithMockUser
    void testAgendarCenario2() throws Exception {
        long idMedico = 2;
        long idPaciente = 5;
        LocalDateTime data = LocalDateTime.now();
        Especialidade especialidade = Especialidade.CARDIOLOGIA;
        DadosAgendamentoConsulta dadosAgendamento = new DadosAgendamentoConsulta(idMedico, idPaciente, data,
                especialidade);
        String jsonEnviado = dadosAgendamentoConsultaJson.write(dadosAgendamento).getJson();
        DadosDetalhamentoConsulta dadosDetalhamento = new DadosDetalhamentoConsulta(null, idMedico, idPaciente, data);
        String jsonEsperado = dadosDetalhamentoConsultaJson.write(dadosDetalhamento).getJson();
        when(agendaDeConsultas.agendar(any())).thenReturn(dadosDetalhamento);

        MockHttpServletResponse response = mvc
                .perform(post("/consultas").contentType(MediaType.APPLICATION_JSON).content(jsonEnviado)).andReturn()
                .getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo(jsonEsperado);
    }
}
