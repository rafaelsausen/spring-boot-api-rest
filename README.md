# API Rest - Spring

Projeto criado utilizando o framework Spring seguindo o curso da Alura - "Spring Boot 3: desenvolva uma API Rest em Java".

## Comando para executar a aplicação

```
    java -Dspring.profiles.active=prod -DDATABASE_HOST=<urlbanco> -DDATABASE_USER=<user> -DDATABASE_PASSWORD=<password> -jar target/api-0.0.1-SNAPSHOT.jar
```
